import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";

const cache = new InMemoryCache();

const Client = new ApolloClient({
  uri: process.env.REACT_APP_API,
  cache
});

export default Client;
