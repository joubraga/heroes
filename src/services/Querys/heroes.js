import gql from "graphql-tag";

export const GET_CHARACTER_BY_ID = gql`
  query hero($id: Int!) {
    characters(where: { id: $id }) {
      name
      resourceURI
      thumbnail
      modified
      name
      description

      urls {
        url
      }

      comics {
        name
      }

      series {
        name
      }

      events {
        name
      }

      stories {
        name
      }
    }
  }
`;

export const GET_CHARACTERS = gql`
  {
    characters {
      id
      name
      thumbnail
      description

      comics {
        name
      }

      series {
        name
      }

      events {
        name
      }

      stories {
        name
      }
    }
  }
`;

export default {
  GET_CHARACTER_BY_ID,
  GET_CHARACTERS
};
