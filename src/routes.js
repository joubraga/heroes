import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Heroes from "./pages/Heroes/List";
import HeroInfo from "./pages/Heroes/Info";
import HeroUpdate from "./pages/Heroes/Update";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Heroes} />
        <Route path="/hero/info/:id" exact component={HeroInfo} />
        <Route path="/hero/update/:id" exact component={HeroUpdate} />
      </Switch>
    </BrowserRouter>
  );
}
