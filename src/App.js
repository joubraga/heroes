import React from "react";
import { Provider } from "react-redux";
import store from "./store";

import client from "./services/apolloClient";
import { ApolloProvider } from "@apollo/react-hooks";

import { SnackbarProvider } from "notistack";
import "./App.css";

import MainLayout from "./pages/Layout/";

function App() {
  return (
    <Provider store={store}>
      <ApolloProvider client={client}>
        <SnackbarProvider maxSnack={3}>
          <MainLayout />
        </SnackbarProvider>
      </ApolloProvider>
    </Provider>
  );
}

export default App;
