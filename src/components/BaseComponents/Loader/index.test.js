import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import BaseComponentLoader from './index';

afterEach(cleanup);

describe('BaseComponent - Loader', () => {
  let Loader;
  beforeEach(async () => {
    const { getByTestId } = render ( <BaseComponentLoader /> );
    Loader = await waitForElement(() => getByTestId('baseComponentLoader'));
  });

  it('Should appear component Loader', async () => {
    expect(Loader).toBeVisible();
  });

  it('Should appear text above the loader', async () => {
    const text = 'Loading ...';
    expect(Loader).toHaveTextContent(text);
  });
});
