import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  grid: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  image: {
    display: "flex",
    flexWrap: "wrap"
  },
  title: {
    fontSize: "20px",
    fontWeight: "bolder"
  }
});

export default function ErrorRequest() {
  const history = useHistory();
  const classes = useStyles();

  function handlerButton() {
    history.goBack();
  }

  return (
    <Grid
      className={classes.grid}
      container
      spacing={2}
      data-testid="baseComponentError"
    >
      <Grid item xs={12}>
        <Typography
          className={classes.title}
          component="h1"
          data-testid="baseComponentErrorTitle"
        >
          Hummmm, looks like we had a problem during the requisition :(
        </Typography>
      </Grid>

      <Grid item xs={12}>
        <img
          className={classes.image}
          src={require("../../../assets/error.gif")}
          alt="Error"
          data-testid="baseComponentErrorImage"
        />
      </Grid>

      <Grid item xs={12}>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={handlerButton}
          data-testid="baseComponentErrorButton"
        >
          Click here to back to previews page
        </Button>
      </Grid>
    </Grid>
  );
}
