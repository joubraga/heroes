import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import BaseComponentImage from './index';

afterEach(cleanup);

describe('BaseComponent - Image', () => {
  let Image;
  const url = 'https://www.indiewire.com/wp-content/uploads/2019/06/joker-movie-fb.jpg?w=768';
  beforeEach(async () => {
    const { getByTestId } = render ( <BaseComponentImage url={url} /> );
    Image = await waitForElement(() => getByTestId('baseComponentImage'));
  });

  it('Should appear component Image', async () => {
    expect(Image).toBeVisible();
  });
});
