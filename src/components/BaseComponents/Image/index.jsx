import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  imageDinamic: {
    width: "100%",
    height: "auto"
  }
});

export default function BaseImage(props) {
  const style = useStyles();

  return (
    <img
      className={style.imageDinamic}
      src={props.url}
      alt="Hero"
      data-testid="baseComponentImage"
    />
  );
}

BaseImage.propTypes = {
  url: PropTypes.string.isRequired
};
