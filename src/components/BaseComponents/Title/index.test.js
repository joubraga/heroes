import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import BaseComponentTitle from './index';

afterEach(cleanup);

describe('BaseComponent - Description', () => {
  let Title;
  const title = "Here's Johnny !";

  beforeEach(async () => {
    const { getByTestId } = render ( <BaseComponentTitle title={title} /> );
    Title = await waitForElement(() => getByTestId('baseComponentTitle'));
  });

  it('Should appear component title', async () => {
    expect(Title).toBeVisible();
  });

  it('Should appear title was passed to prop', async() => {
    expect(Title).toHaveTextContent(title);
  });
});
