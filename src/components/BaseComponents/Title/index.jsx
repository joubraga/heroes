import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles({
  spacer: {
    margin: "20px 0"
  }
});

export default function BaseTitle(props) {
  const classes = useStyles();
  return (
    <Typography
      className={classes.spacer}
      component="h2"
      data-testid="baseComponentTitle"
    >
      <Box fontWeight="fontWeightBold">{props.title}</Box>
    </Typography>
  );
}

BaseTitle.propTypes = {
  title: PropTypes.string.isRequired
};
