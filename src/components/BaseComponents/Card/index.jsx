import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  a: {
    textDecoration: "none !important"
  },
  card: {
    maxWidth: 345
  },
  media: {
    height: 275,
    backgroundSize: "cover",
    backgroundPosition: "initial"
  }
});

export default function BaseCard(props) {
  const classes = useStyles();

  function showDescription() {
    if (!props.justName) {
      return (
        <Typography variant="body2" color="textSecondary" component="p">
          {props.data.description}
        </Typography>
      );
    }
  }

  function showName() {
    if (!props.data.title) {
      return props.data.name;
    }

    return props.data.title;
  }

  function callActionButton() {
    const seeMoreText = "See more ...";

    return (
      <Link
        className={classes.a}
        to={`/${props.routerPath}/info/${props.data.id}`}
      >
        <Button
          data-testid="baseComponentCardSeeMore"
          variant="contained"
          color="primary"
          size="small"
        >
          {seeMoreText}
        </Button>
      </Link>
    );
  }

  function callActionEditButton() {
    const editButtonText = "Edit Hero";

    return (
      <Link className={classes.a} to={`/hero/update/${props.data.id}`}>
        <Button
          className="buttonEditHero"
          variant="contained"
          color="primary"
          size="small"
        >
          {editButtonText}
        </Button>
      </Link>
    );
  }

  return (
    <Card className={classes.card} data-testid="baseComponentCard">
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={props.data.thumbnail}
          title={props.data.title}
        />

        <CardContent data-testid="baseComponentCardContent">
          <Typography gutterBottom component="h2">
            {showName()}
          </Typography>

          {showDescription()}
        </CardContent>
      </CardActionArea>

      <CardActions data-testid="baseComponentCardActions">
        {callActionButton()}

        {callActionEditButton()}
      </CardActions>
    </Card>
  );
}

BaseCard.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string,
  justName: PropTypes.bool,
  callActionById: PropTypes.bool,
  routerPath: PropTypes.string,
  thumbnail: PropTypes.string
};
