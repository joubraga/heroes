import React from "react";
import { render, waitForElement, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import BaseComponentDescription from "./index";

afterEach(cleanup);

describe("BaseComponent - Description", () => {
  it("Should appear component description", async () => {
    const { getByTestId } = render(<BaseComponentDescription />);

    const description = await waitForElement(() =>
      getByTestId("baseComponentDescription")
    );
    expect(description).toBeVisible();
  });

  it("Should appear default message when no has description", async () => {
    const { getByTestId } = render(<BaseComponentDescription />);
    const description = await waitForElement(() =>
      getByTestId("baseComponentDescription")
    );

    expect(description).toHaveTextContent("This hero has no description");
  });

  it("Should appear description was passed to prop", async () => {
    const text = "Testing description";
    const { getByTestId } = render(
      <BaseComponentDescription description={text} />
    );
    const description = await waitForElement(() =>
      getByTestId("baseComponentDescription")
    );

    expect(description).toHaveTextContent(text);
  });
});
