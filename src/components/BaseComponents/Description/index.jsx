import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  spacer: {
    margin: "20px 0"
  }
});

export default function BaseDescription(props) {
  const classes = useStyles();

  function hasDescription() {
    if (!props.description) {
      return "This hero has no description";
    }
    return props.description;
  }

  return (
    <>
      <Typography
        className={classes.spacer}
        data-testid="baseComponentDescription"
        component="p"
      >
        {hasDescription()}
      </Typography>
    </>
  );
}

BaseDescription.propTypes = {
  description: PropTypes.string
};
