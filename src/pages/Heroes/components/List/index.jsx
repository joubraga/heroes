import React from "react";
import PropTypes from "prop-types";

import BaseCard from "../../../../components/BaseComponents/Card";
import BaseTitle from "../../../../components/BaseComponents/Title";
import Grid from "@material-ui/core/Grid";

export default function HeroesList(props) {
  function showHero() {
    if (props.heroesList && props.heroesList.length > 0) {
      return (
        <Grid container spacing={3} data-testid="heroList">
          {props.heroesList.map(hero => (
            <Grid item xs={12} md={4} lg={4} key={hero.id}>
              <BaseCard
                key={hero.id}
                data={hero}
                routerPath="hero"
                justName={true}
                callActionById={true}
              />
            </Grid>
          ))}
        </Grid>
      );
    }

    return <BaseTitle title={`Hero "${props.hero}" not found.`} />;
  }

  return <>{showHero()}</>;
}

HeroesList.propTypes = {
  heroesList: PropTypes.array.isRequired,
  hero: PropTypes.string
};
