import React from "react";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightBold
  },
  spacer: {
    margin: "20px 0"
  }
}));

export default function HeroesInfoList(props) {
  const classes = useStyles();
  const titleClass = `${classes.heading} expansion-panel-title`;

  return (
    <ExpansionPanel className={classes.spacer}>
      <ExpansionPanelSummary
        id="panel1a-header"
        className="expansion-panel"
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
      >
        <Typography className={titleClass}>{props.title}</Typography>
      </ExpansionPanelSummary>

      {props.data &&
        props.data.map((info, index) => (
          <ExpansionPanelDetails key={index}>
            <Typography>{info.name}</Typography>
          </ExpansionPanelDetails>
        ))}
    </ExpansionPanel>
  );
}

HeroesInfoList.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.array
};
