import React, { useState } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { GET_CHARACTER_BY_ID } from "../../../../services/Querys/heroes";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as HeroesActions } from "../../../../store/ducks/heroes";

import BaseImage from "../../../../components/BaseComponents/Image";
import BaseTitle from "../../../../components/BaseComponents/Title";
import BaseDescription from "../../../../components/BaseComponents/Description";
import Loader from "../../../../components/BaseComponents/Loader";
import Error from "../../../../components/BaseComponents/Error";
import InfoList from "./list";

function HeroInfo(props) {
  const [hero, setHero] = useState("");
  const { id } = useParams();

  const { loading, error } = useQuery(GET_CHARACTER_BY_ID, {
    variables: {
      id: parseInt(id, 10)
    },
    onCompleted: e => {
      if (!props.heroes) {
        setHero(e.characters[0]);
      } else {
        const filtredHero = props.heroes.filter(hero => hero.id === id);

        if (filtredHero) {
          setHero(filtredHero[0]);
        }
      }
    }
  });

  if (loading) return <Loader />;
  if (error) return <Error />;

  function showHero() {
    if (hero) {
      return (
        <div className="animated fadeIn">
          <BaseImage url={hero.thumbnail} />

          <BaseTitle title={hero.name} />

          <BaseDescription description={hero.description} />

          <InfoList title="Comics" data={hero.comics} />

          <InfoList title="Series" data={hero.series} />

          <InfoList title="Stories" data={hero.stories} />

          <InfoList title="Events" data={hero.events} />
        </div>
      );
    }

    return <BaseTitle title="Hero not Found." />;
  }

  return <>{showHero()}</>;
}

const mapStateToProps = state => ({
  heroes: state.heroes.heroesList
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(HeroesActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeroInfo);

HeroInfo.propTypes = {
  heroes: PropTypes.array,
  heroesList: PropTypes.func,
  setHeroesList: PropTypes.func,
  showAlertSuccess: PropTypes.func
};
