import React, { useState } from "react";
import PropTypes from "prop-types";
import { useParams, useHistory } from "react-router-dom";

import { useQuery } from "@apollo/react-hooks";
import { GET_CHARACTER_BY_ID } from "../../../../services/Querys/heroes";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as HeroesActions } from "../../../../store/ducks/heroes";

import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

import BaseTitle from "../../../../components/BaseComponents/Title";
import Loader from "../../../../components/BaseComponents/Loader";
import Error from "../../../../components/BaseComponents/Error";

import SaveIcon from "@material-ui/icons/Save";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "100%"
  },
  button: {
    margin: theme.spacing(1)
  }
}));

function HeroForm(props) {
  const history = useHistory();
  const { id } = useParams();
  const classes = useStyles();

  const [hero, setHero] = useState({
    name: "",
    description: "",
    thumbnail: "",
    comics: [],
    series: [],
    stories: []
  });

  const [validForm, setValidForm] = useState({
    name: true
  });

  const { loading, error } = useQuery(GET_CHARACTER_BY_ID, {
    variables: {
      id: parseInt(id, 10)
    },
    onCompleted: e => {
      if (!props.heroes) {
        setHero({
          name: e.characters[0].name,
          description: e.characters[0].description,
          thumbnail: e.characters[0].thumbnail,
          comics: e.characters[0].comics,
          series: e.characters[0].series,
          stories: e.characters[0].stories
        });
      } else {
        const filtredHero = props.heroes.filter(hero => hero.id === id);

        if (filtredHero) {
          setHero({
            name: filtredHero[0].name,
            description: filtredHero[0].description,
            thumbnail: filtredHero[0].thumbnail,
            comics: filtredHero[0].comics,
            series: filtredHero[0].series,
            stories: filtredHero[0].stories
          });
        }
      }
    }
  });

  if (loading) return <Loader />;
  if (error) return <Error />;

  function isValidForm() {
    if (hero.name === "") {
      setValidForm({
        name: false
      });

      return false;
    }

    return true;
  }

  function handlerSave() {
    if (isValidForm()) {
      if (props.heroes) {
        const newListHero = props.heroes.map(h => {
          if (h.id === id) {
            hero.id = id;
            return hero;
          }
          return h;
        });

        props.setHeroesList(newListHero);

        props.showAlertSuccess(true);
        history.push("/");
      }
    }
  }

  function showForm() {
    if (hero) {
      const classInputName = `${classes.textField} hero-input-name`;
      const classInputDescription = `${classes.textField} hero-input-description`;
      const classInputUrl = `${classes.textField} hero-input-url`;
      const classButton = `${classes.button} hero-button-save`;

      return (
        <div className="animated fadeIn">
          <form className={classes.container} noValidate autoComplete="off">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  label={validForm.name ? "Name" : "Error"}
                  type="text"
                  className={classInputName}
                  margin="normal"
                  variant="outlined"
                  error={validForm.name ? false : true}
                  helperText="Required field."
                  onChange={e =>
                    setHero({
                      ...hero,
                      name: e.target.value
                    })
                  }
                  value={hero.name}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  className={classInputDescription}
                  label="Description"
                  type="textarea"
                  multiline
                  rows="4"
                  margin="normal"
                  variant="outlined"
                  onChange={e =>
                    setHero({
                      ...hero,
                      description: e.target.value
                    })
                  }
                  value={hero.description}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  className={classInputUrl}
                  label="Image Hero"
                  type="textarea"
                  multiline
                  rows="4"
                  margin="normal"
                  variant="outlined"
                  onChange={e =>
                    setHero({
                      ...hero,
                      thumbnail: e.target.value
                    })
                  }
                  value={hero.thumbnail}
                />
              </Grid>

              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  className={classButton}
                  startIcon={<SaveIcon />}
                  onClick={handlerSave}
                >
                  Save
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
      );
    }

    return (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          ,
          <BaseTitle title="Hero not Found." />
        </Grid>
      </Grid>
    );
  }

  return <>{showForm()}</>;
}

const mapStateToProps = state => ({
  heroes: state.heroes.heroesList,
  showAlertSuccess: state.heroes.showAlertSuccess
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(HeroesActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeroForm);

HeroForm.propTypes = {
  heroes: PropTypes.array,
  setHeroesList: PropTypes.func.isRequired,
  showAlertSuccess: PropTypes.func.isRequired
};
