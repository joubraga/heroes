import React from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  textField: {
    width: '100%',
  },
}));

export default function Search(props) {
  const classes = useStyles();

  async function handlerText(e) {
    props.handlerSearch(e.target.value);
  }

  return (
    <TextField
      data-testid="heroComponentSearch"
      id="outlined-search"
      label="Search Hero"
      type="search"
      className={classes.textField}
      margin="normal"
      variant="outlined"
      onChange={handlerText}
    />
  )
};

Search.propTypes = {
  handlerSearch: PropTypes.func.isRequired,
};
