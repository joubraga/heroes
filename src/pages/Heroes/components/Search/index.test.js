import React from 'react';
import { render, waitForElement, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import HeroSearch from './index';

afterEach(cleanup);

describe('BaseComponent - Search', () => {
  let search;

  beforeEach(async () => {
    const handlerSerach = () => console.log('Return values to component father');
    const { getByTestId } = render ( <HeroSearch handlerSearch={handlerSerach} /> );
    search = await waitForElement(() => getByTestId('heroComponentSearch'));
  });

  it('Should appear component Search', async () => {
    expect(search).toBeVisible();
  });

});
