import React, { useState, useEffect } from "react";
import { withSnackbar } from "notistack";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as HeroesActions } from "../../../store/ducks/heroes";

import { useQuery } from "@apollo/react-hooks";
import { GET_CHARACTERS } from "../../../services/Querys/heroes";

import SarchHeroes from "./Search";
import ListHeroes from "./List";
import Loader from "../../../components/BaseComponents/Loader";
import Error from "../../../components/BaseComponents/Error";

function Heroes(props) {
  const [searchText, setSearcText] = useState("");

  useEffect(() => {
    if (props.heroes) {
      getList();
    }
  });

  useEffect(() => {
    if (props.showAlertHeroSuccess) {
      const message = "Hero updated with success.";

      props.enqueueSnackbar(message, {
        variant: "success",
        autoHideDuration: 2500
      });

      props.showAlertSuccess(false);
    }
  });

  const { loading, error } = useQuery(GET_CHARACTERS, {
    onCompleted: e => {
      if (!props.heroes && e.characters) {
        props.setHeroesList(e.characters);
      }
    }
  });

  if (loading) return <Loader />;
  if (error) return <Error />;

  function handlerSearch(text) {
    setSearcText(text);
  }

  function getList() {
    if (searchText) {
      const hero = props.heroes.filter(hero =>
        hero.name.toLowerCase().includes(searchText.toLowerCase())
      );

      if (hero) {
        return hero;
      }
    }
    return props.heroes;
  }

  function showList() {
    if (props.heroes && props.heroes.length > 0) {
      return <ListHeroes heroesList={getList()} hero={searchText} />;
    }
  }

  return (
    <>
      <SarchHeroes handlerSearch={handlerSearch} />
      {showList()}
    </>
  );
}

const mapStateToProps = state => ({
  heroes: state.heroes.heroesList,
  searchText: state.heroes.searchText,
  showAlertHeroSuccess: state.heroes.showAlertHeroSuccess
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(HeroesActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(Heroes));
