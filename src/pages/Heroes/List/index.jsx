import React from "react";
import Container from "@material-ui/core/Container";

import Heroes from "../components/";

export default function HeroesList() {
  return (
    <>
      <Container className="animated fadeInUp">
        <Heroes />
      </Container>
    </>
  );
}
