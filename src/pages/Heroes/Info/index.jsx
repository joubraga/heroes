import React from "react";

import HeroInfo from "../components/Info";
import Container from "@material-ui/core/Container";

export default function Info() {
  return (
    <Container>
      <HeroInfo />
    </Container>
  );
}
