import { createActions, createReducer } from "reduxsauce";

export const { Types, Creators } = createActions({
  setHeroesList: ["characters"],
  heroesList: [],
  showAlertSuccess: ["isVisible"]
});

const INITIAL_STATE = [
  {
    showAlertHeroSuccess: false
  }
];

const setHeroesList = (state = INITIAL_STATE, action) => ({
  ...state,
  heroesList: action.characters
});

const setShowAlertSuccess = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    showAlertHeroSuccess: action.isVisible
  };
};

export default createReducer(INITIAL_STATE, {
  [Types.SET_HEROES_LIST]: setHeroesList,
  [Types.SHOW_ALERT_SUCCESS]: setShowAlertSuccess
});
