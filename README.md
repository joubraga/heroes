# Desafio Ténico Softplan - Jonathan Braga

Faaaaaaaala dev beleza  ?

Caso queira ver a aplicação pelo Heroku clique aqui: [https://heroes-marvel-softplan.herokuapp.com/](https://heroes-marvel-softplan.herokuapp.com/)

Irei lhe passar as instruções caso queira, executar o projeto no local de sua máquina.

# Steps
Primeiramente faça o clone do meu repositório:  https://gitlab.com/joubraga/heroes.git
Logo após o término do download do repositório, acesse a pasta e instale as dependências com o seu gerenciador de sua preferência.

Comandos:

    yarn install

	or

    npm install

Feito isso será instalados as dependências necessárias para rodar este projeto.

Agora crie o arquivo **.env**  na pasta raiz do projeto, ou seja, fora da pasta **src**
Adicione o seguinte texto:

    REACT_APP_API=https://api.marvelql.com/

> OBS: Sem a criação deste arquivo **não irá funcionar as requisições.**

Feito assim, iremos dar o start em nossa aplicação através do comando:

    yarn start

    or

    npm run start

Pronto a aplicação irá abrir em seu navegador com a url:  http://localhost:3000

> Obs: Caso você tenha algum serviço rodando nesta porta, a aplicação não irá subir.

Caso  queira ver os testes unitários execute o comando:

    yarn test

    or

	npm run test

Caso queira ver os teste de interface (E2E) execute o comando:

    yarn cypress

    or

	npm run cypress
