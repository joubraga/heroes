describe('Heroes Info', () => {
  before(() => {
    cy.visit('/');
  });

  it('Should be possible click in button See More about one hero', () => {
    cy.get('[data-testid=baseComponentCard] :nth-child(1)')
      .find('[data-testid=baseComponentCardSeeMore]')
      .eq(0)
      .click()
  });

  it('Should appear the hero image', () => {
    cy.wait(1000);

    cy.get('[data-testid=baseComponentImage]')
      .should('be.visible')
  });

  it('Should appear the hero name', () => {
    cy.wait(1000);

    cy.get('[data-testid=baseComponentTitle] > div')
      .should('be.visible')
      .contains('3-D Man');
  });

  it('Should be possible see description hero', () => {
    cy.get('[data-testid=baseComponentDescription]')
      .should('be.visible')
      .contains('This hero has no description');
  });

  it('Should be possible click to open expansel panel Comics', () => {
    cy.get('.expansion-panel-title')
      .eq(0)
      .click({ force: true });
  });

  it('Should be possible click to close expansel panel Comics', () => {
    cy.get('.expansion-panel-title')
      .eq(0)
      .click({ force: true });
  });

  it('Should be possible click to open expansel panel Series', () => {
    cy.get('.expansion-panel-title')
      .eq(1)
      .click({ force: true });
  });

  it('Should be possible click to close expansel panel Series', () => {
    cy.get('.expansion-panel-title')
      .eq(1)
      .click({ force: true });
  });

  it('Should be possible click to open expansel panel Stories', () => {
    cy.get('.expansion-panel-title')
      .eq(2)
      .click({ force: true });
  });

  it('Should be possible click to close expansel panel Stories', () => {
    cy.get('.expansion-panel-title')
      .eq(2)
      .click({ force: true });
  });

  it('Should be possible click to open expansel panel Events', () => {
    cy.get('.expansion-panel-title')
      .eq(3)
      .click({ force: true });
  });

  it('Should be possible click to close expansel panel Events', () => {
    cy.get('.expansion-panel-title')
      .eq(3)
      .click({ force: true });
  });

  it('Should be possible back to Heroes List click in Menu item List', () => {
    cy.get('[data-testid=drawerMenuItem] > .MuiButtonBase-root')
      .click();
  });

  it('Should be possible click in button Edit Hero', () => {
    cy.get('[data-testid=baseComponentCard]')
      .find('.buttonEditHero')
      .eq(0)
      .click();
  });

  it('Should be possible change name hero', () => {
    cy.get('.hero-input-name > div > input')
      .should('be.visible')
      .clear()
      .type('Deadpool')
  });

  it('Should be possible change description hero', () => {
    cy.get('.hero-input-description > div > textarea')
      .should('be.visible')
      .clear()
      .type('Deadpool é um personagem fictício do universo Marvel, que atua geralmente como anti-herói e ocasionalmente como vilão. Deadpool,'
      + ' cujo nome verdadeiro é Wade Winston Wilson, é um mercenário canadense marcado por ser falastrão e violento. Tem também o fator de cura '
      + 'que o faz sobreviver aos piores ferimentos.');
  });

  it('Should be possible change image hero', () => {
    cy.get('.hero-input-url > div > textarea')
      .should('be.visible')
      .clear()
      .type('http://images.universohq.com//2016/02/DeadpoolDiaNamorados_des.jpg');
  });

  it('Should be possible click in button save hero', () => {
    cy.get('.hero-button-save')
      .should('be.visible')
      .click();
  });

  it('Should be visible snackBar success after update hero', () => {
    cy.get('.MuiSnackbar-root > .MuiTypography-root').should('be.visible');
  });

  it('Should have Deadpool in first position list', () => {
    cy.get('[data-testid=baseComponentCard] :nth-child(1)')
      .find('[data-testid=baseComponentCardContent] > h2')
      .eq(0)
      .contains('Deadpool');
  });

  it('When update page should not exist Deadpool in heroes list', () => {
    cy.visit('/');

    cy.get('[data-testid=baseComponentCard] :nth-child(1)')
      .find('[data-testid=baseComponentCardContent] > h2')
      .eq(0)
      .contains('Deadpool')
      .should('not.exist');
  })
});
