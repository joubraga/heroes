describe('Heros List', () => {
  before(() => {
    cy.visit('/');
  });

  it('Should appear List Heroes page', () => {
    cy.get('.MuiToolbar-root > .MuiTypography-root')
      .contains('Heroes');
  });

  it('The list heroes should be visible', () => {
    cy.get('[data-testid=baseComponentCard]')
      .its('length')
      .should('be.gt', 3)
  });
});

