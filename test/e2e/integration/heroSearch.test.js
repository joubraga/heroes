describe('Heros Search', () => {
  before(() => {
    cy.visit('/');
  });

  it('Should be visible input search heroes', () => {
    cy.wait(500);
    cy.get('[data-testid=heroComponentSearch]').should('be.visible');
  });

  it('Should be possible type in input search', () => {
    cy.get('#outlined-search').type('3-D Man');

    cy.get('[data-testid=heroList]')
      .find('[data-testid=baseComponentCardContent] > .MuiTypography-root')
  });

  it('The hero not found message should appear when typing a non-existent hero', () => {
    const heroName = 'bla bla';
    cy.wait(500);
    cy.get('#outlined-search')
      .clear()
      .type(heroName);

    cy.get('.MuiBox-root').contains(`Hero "${heroName}" not found`);
  });
});

